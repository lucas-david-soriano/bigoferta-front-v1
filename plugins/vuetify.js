import Vue from 'vue'
import {
  Vuetify,
  VApp,
  VCard,
  VCarousel,
  VImg,
  VRating,
  VChip,
  VNavigationDrawer,
  VFooter,
  VList,
  VBtn,
  VIcon,
  VGrid,
  VTextField,
  VToolbar
} from 'vuetify'

Vue.use(Vuetify, {
  components: {
    VApp,
    VCard,
    VCarousel,
    VImg,
    VRating,
    VChip,
    VNavigationDrawer,
    VFooter,
    VList,
    VBtn,
    VIcon,
    VGrid,
    VTextField,
    VToolbar
  }
})
